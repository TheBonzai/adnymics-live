import pytest
from recommendation_service import *
from game_service import GameService


class TestRateMoveChooseMine:
    @pytest.mark.parametrize("position,expected", [
        (1, (3, 13)),
        (2, (6, 28)),
        (3, (5, 27)),
        (4, (4, 26)),
        (47, (3, 20))
    ])
    def test_use_ladders(self, mocker, position, expected):
        mocker.patch.object(GameService, "get_position_current_player", lambda: position)

        assert (rate_me_choose_dice() == expected)

    def test_multiple_snakes(self, mocker):
        mocker.patch.object(GameService, "get_position_current_player", lambda: 93)

        assert (rate_me_choose_dice() == (6, 6))


class TestRateMoveFreeEnemy:
    def test_multiple_ladders(self, mocker):
        mocker.patch.object(GameService, "get_position_other_player", lambda: 0)

        expect = ((1 + 3 + 10) + (2 + 6 + 22) + (3 + 5 + 22) + (4 + 4 + 22) + (5 + 3 + 22) + (6 + 2 + 22)) / 6
        assert (rate_enemy_free_dice() == expect)


class TestRateMoveRandomMine:
    def test_multiple_ladders(self, mocker):
        mocker.patch.object(GameService, "get_position_current_player", lambda: 2)

        expect = (1 + (2 + 10) + 3 + 4 + 5 + (6 + 22)) / 6
        assert (rate_my_random_dice() == expect)

    def test_multiple_snakes(self, mocker):
        mocker.patch.object(GameService, "get_position_current_player", lambda: 31)

        expect = ((1 - 22) + 2 + 3 + 4 + (5 - 30) + 6) / 6
        assert (rate_my_random_dice() == expect)


class TestRateMoveChooseEnemy:
    def test_skip_ladders(self, mocker):
        mocker.patch.object(GameService, "get_position_other_player", lambda: 3)

        expect = (5, 11)
        assert (rate_choose_enemy_dice() == expect)
