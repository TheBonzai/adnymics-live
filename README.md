# (Live) Challenge

The two cases to distinguish with two dice each are:

Player | Attack move | Safe move
  ---  |     ---     |    ---
Current | predefined + random | Maximize( predefined + CHOOSE(1-6) ) 
Other | Minimize ( CHOOSE(1-6) + choose best ) | random + choose best

For the attack move there is for the current a predefined die and a random die.
With the attack move one tries delay the other player before approaching a ladder 
(see turn 3.0 and 3.5 below) or denying a ladder by putting the opponent directly on
a ladder so with the second die the ladder cannot be used (see turn 4.0).

With the safe move the player tries to use as much ladders as possible or advance
with large die values.

To decide which move to use the expected/mean value of each cell is calculated.
Then for each case the value for the current player is subtracted by the value
of the other player. 
With that both the own gain and the possible gain for the other player is 
considered.
These differences of both cases are compared and the case with the larger 
difference is chosen.

#### Comment
In the recommendations it is assumed that the other player in any case will choose his second die (see table). 
This is not valid anymore if the recommendation of the other player is to choose
the die of the current player and not to choose his own die in the next turn.
Besides, advanced strategies involving using snakes to return to passed ladders
are not considered either.


Example output of a game between two bots following all recommendations:
```
=== TURN 1 ===
Player 0 initially move 2 steps: now on 2
Use strategy SAFE and chosen move self 6 steps
USE LADDER
now on 30
=== TURN 1.5 ===
Player 1 initially move 5 steps: now on 5
Use strategy SAFE and chosen move self 3 steps
USE LADDER
now on 30
=== TURN 2.0 ===
Player 0 initially move 1 steps: now on 31
Use strategy SAFE and chosen move self 6 steps
now on 37
=== TURN 2.5 ===
Player 1 initially move 3 steps: now on 33
Use strategy SAFE and chosen move self 6 steps
now on 39
=== TURN 3.0 ===
Player 0 initially move 1 steps: now on 38
Use strategy ATTACK and force enemy 1 steps / random move self 4 steps
now on 42
=== TURN 3.5 ===
Player 1 initially move 1 steps: now on 40
Use strategy ATTACK and force enemy 1 steps / random move self 4 steps
now on 44
=== TURN 4.0 ===
Player 0 initially move 1 steps: now on 43
Use strategy ATTACK and force enemy 6 steps / random move self 4 steps
now on 47
=== TURN 4.5 ===
Player 1 initially move 6 steps: now on 50
Use strategy ATTACK and force enemy 3 steps / random move self 6 steps
now on 56
=== TURN 5.0 ===
Player 0 initially move 3 steps: now on 50
Use strategy SAFE and chosen move self 6 steps
now on 56
=== TURN 5.5 ===
Player 1 initially move 4 steps: now on 60
Use strategy SAFE and chosen move self 6 steps
now on 66
=== TURN 6.0 ===
Player 0 initially move 2 steps: now on 58
Use strategy ATTACK and force enemy 5 steps / random move self 1 steps
now on 59
=== TURN 6.5 ===
Player 1 initially move 5 steps: now on 71
Use strategy ATTACK and force enemy 1 steps / random move self 4 steps
now on 75
=== TURN 7.0 ===
Player 0 initially move 1 steps: now on 60
Use strategy SAFE and chosen move self 6 steps
now on 66
=== TURN 7.5 ===
Player 1 initially move 1 steps: now on 76
Use strategy SAFE and chosen move self 4 steps
USE LADDER
now on 99
=== TURN 8.0 ===
Player 0 initially move 4 steps: now on 70
Use strategy SAFE and chosen move self 1 steps
USE LADDER
now on 92
=== TURN 8.5 ===
Player 1 initially move 4 steps: now on 103
Use strategy SAFE and chosen move self 6 steps
now on 109

Player 1 has won!
```