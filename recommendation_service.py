from typing import List, Tuple, Union
from game_service import GameService, Strategies
from statistics import mean

ChoiceRating = Tuple[int, Union[int, float]]


def recommend_next() -> (Strategies, int):
    """
    Returns a recommendation tuple. The first element is the Strategy and the second element
    is the steps to move the current or enemy player.

    :rtype: (Strategies, int)
    """
    atk_steps, atk_rating = attack_move()
    safe_steps, safe_rating = safe_move()

    if safe_rating >= atk_rating:
        return Strategies.SAFE, safe_steps
    else:
        return Strategies.ATTACK, atk_steps


def rate_me_choose_dice() -> ChoiceRating:
    """
    Evaluate case where player chooses own die

    :rtype: ChoiceRating
    """
    my_position = GameService.get_position_current_player()
    return find_best_step(my_position)


def rate_enemy_free_dice() -> float:
    """
    Evaluate case where the first die from the enemy is not manipulated

    :rtype: float
    """
    enemy_position = GameService.get_position_other_player()
    best_step_values = [step + find_best_step(enemy_position + step)[1] for step in range(1, 7)]
    return mean(best_step_values)


def rate_my_random_dice() -> int:
    """
    Evaluate case where both own dice are random

    :rtype: int
    """
    my_position = GameService.get_position_current_player()
    return mean(rate_next_steps(my_position))


def rate_choose_enemy_dice() -> ChoiceRating:
    """
    Evaluate case where current player manipulates first die of the enemy

    :rtype: ChoiceRating
    """
    enemy_position = GameService.get_position_other_player()
    steps_value = [step + find_best_step(enemy_position + step)[1] for step in range(1, 7)]

    min_rating = min(steps_value)
    step = steps_value.index(min_rating) + 1
    return step, min_rating


def safe_move() -> ChoiceRating:
    """
    Evaluate a safe move, where player choose one own die and enemy player is not affected

    :rtype: ChoiceRating
    """

    # TODO check if current player can win, if yes: force safe move

    step, gain = rate_me_choose_dice()
    enemy_gain = rate_enemy_free_dice()

    return step, gain - enemy_gain


def attack_move() -> ChoiceRating:
    """
    Evaluate an attack move, where first enemy die is manipulated, but both own dice are random.

    :rtype: ChoiceRating
    """
    gain = rate_my_random_dice()
    step, enemy_gain = rate_choose_enemy_dice()

    return step, gain - enemy_gain


def find_best_step(position: int) -> ChoiceRating:
    """
    Find best step in the reach of one die

    :type position: int
    :param position: current position for further evaluation
    :rtype: ChoiceRating
    """
    ratings = rate_next_steps(position)
    max_rating = max(ratings)
    step = ratings.index(max_rating) + 1
    return step, max_rating


def rate_next_steps(position: int) -> List[int]:
    """
    Return list with rating of the steps in reach of one die

    :type position: int
    :param position: current position for further evaluation
    :rtype: List[int]
    """
    return [rate_step(position, step) for step in range(1, 7)]


def rate_step(position: int, step: int) -> int:
    """
    Return the rating for the given position and step including ladders and snakes

    :type position: int
    :param position: current position for further evaluation
    :type step: int
    :param step: step to advance from `position`
    :rtype: int
    """

    next_pos = position + step
    if next_pos in GameService.board:
        diff = GameService.board[next_pos] - next_pos + step
        return diff
    else:
        return step
