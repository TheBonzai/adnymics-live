from recommendation_service import recommend_next
from game_service import GameService, Strategies
from random import randint, seed


class Game:
    _winner = None
    _next_dice = None

    def start_loop(self):
        seed(1)
        turn = 1
        while Game._winner is None:
            print(f"=== TURN {turn} ===")
            self.eval_first_dice()
            self.make_move()
            self.end_turn()
            turn += 0.5

        print()
        print(f"Player {self._winner} has won!")

    def eval_first_dice(self):
        step = self._next_dice if self._next_dice else randint(1, 6)
        GameService.move_player_position(GameService.get_current_player(), step, False)
        print(
            f"Player {GameService.get_current_player()} initially move {step} steps: "
            f"now on {GameService.get_position_current_player()}")

    def make_move(self):
        strategy, chosen_step = recommend_next()
        if strategy == Strategies.SAFE:
            print(f"Use strategy SAFE and chosen move self {chosen_step} steps")
            GameService.move_player_position(GameService.get_current_player(), chosen_step, True)
            self._next_dice = None
        else:  # strategy == Strategies.ATTACK
            step = randint(1, 6)
            print(f"Use strategy ATTACK and force enemy {chosen_step} steps / random move self {step} steps")
            GameService.move_player_position(GameService.get_current_player(), step, True)
            self._next_dice = chosen_step

        print(f"now on {GameService.get_position_current_player()}")

    def end_turn(self):
        if GameService.get_position_current_player() >= 100:
            Game._winner = GameService.get_current_player()
        else:
            GameService.switch_player()
