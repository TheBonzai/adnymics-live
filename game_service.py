from enum import Enum


class Strategies(Enum):
    SAFE = 0
    ATTACK = 1


class GameService:
    _current_player = 0
    _player_positions = {
        1: 0,
        0: 0,
    }

    board = {
        1: 38,
        4: 14,
        8: 30,
        21: 42,
        28: 76,
        50: 67,
        71: 92,
        80: 99,
        97: 78,
        95: 56,
        88: 24,
        62: 18,
        48: 26,
        36: 6,
        32: 10,
    }

    @staticmethod
    def get_position_current_player() -> int:
        return GameService._get_player_position(GameService.get_current_player())

    @staticmethod
    def get_position_other_player() -> int:
        return GameService._get_player_position(GameService._get_other_player())

    @staticmethod
    def move_player_position(player_no: int, step: int, allow_jumps: bool):
        next_pos = GameService._get_player_position(player_no) + step
        if allow_jumps and next_pos in GameService.board:
            if GameService.board[next_pos] > next_pos:
                print("USE LADDER")
            if GameService.board[next_pos] < next_pos:
                print("USE SNAKE")

            GameService._set_player_position(player_no, GameService.board[next_pos])
        else:
            GameService._set_player_position(player_no, next_pos)

    @staticmethod
    def switch_player():
        GameService._current_player = GameService._get_other_player()

    @staticmethod
    def get_current_player():
        return GameService._current_player

    @staticmethod
    def _get_other_player():
        return 0 if GameService._current_player == 1 else 1

    @staticmethod
    def _get_player_position(player_no: int) -> int:
        return GameService._player_positions[player_no]

    @staticmethod
    def _set_player_position(player_no: int, position: int):
        GameService._player_positions[player_no] = position
